import React, { FC } from 'react'

type Props = {
    weatherList: any,
    days: number,
    changeDays(event: React.FormEvent<HTMLInputElement>): void
}

const WeatherList: FC<Props> = ({ weatherList, days, changeDays}) => {
    return (
        <div>
            <label htmlFor="days">Select the days to display:</label>
            <input type="number" className="form-control" id="days" name="days" min="1" max="5" value={days} onChange={changeDays}/>

            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Date/Time</th>
                        <th>Temperature</th>
                        <th>Humidity</th>
                    </tr>
                </thead>
                <tbody id="tableBody">
                    {
                        weatherList.map((weatherList: { dt_txt: React.ReactNode; main: { humidity: React.ReactNode; temp: React.ReactNode; }; }) => (
                            <tr>
                                <td>{weatherList.dt_txt}</td>
                                <td>{weatherList.main.temp}&deg;</td>
                                <td>{weatherList.main.humidity}%</td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>

        </div>
    )
};

export default WeatherList
