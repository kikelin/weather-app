import React, {FC} from 'react';
import {Switch, Route} from 'react-router-dom'
import Chihuahua from '../../containers/Chihuahua/Chihuahua';
import Juarez from '../../containers/Juarez/Juarez';
import Parral from '../../containers/Parral/Parral';

const Routes: FC = () => {
    return (
        <Switch>
            <Route path="/" component={Chihuahua} exact/>
            <Route path="/chihuahua" component={Chihuahua}/>
            <Route path="/juarez" component={Juarez}/>            
            <Route path="/parral" component={Parral}/>
        </Switch>
    );
}

export default Routes;
