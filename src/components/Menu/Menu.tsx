import React, { SFC } from 'react';
import { Link } from 'react-router-dom';

const Header: SFC = () => {
    return (
        <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
            <Link className="navbar-brand" to="/">Weather Forecast</Link>
            <ul className="navbar-nav">
                <li className="nav-item">
                    <Link className="nav-link" to="/chihuahua">Chihuahua</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/juarez">Juarez</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/parral">Parral</Link>
                </li>
            </ul>
        </nav>
    )
}

export default Header;
