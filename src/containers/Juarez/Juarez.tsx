import React, {Component} from 'react';
import WeatherList from '../../components/WeatherList/WeatherList';

class Juarez extends Component {
    state = {
        weatherList: [] as any,
        days: 1
    }

    public render() {
        return (
            <WeatherList weatherList={this.state.weatherList} days={this.state.days} changeDays={this.changeDays}/>
        );
    }

    componentDidMount() {
        var cnt = this.state.days * 8;

        fetch('http://api.openweathermap.org/data/2.5/forecast?id=4013708&appid=7a147d89f0a5ffb402d041e6623e2778&cnt=' + cnt)
            .then(res => res.json())
            .then((data) => {
                this.setState({ weatherList: data.list })
            })
            .catch(console.log)
    }

    componentDidUpdate() {
        var cnt = this.state.days * 8;

        fetch('http://api.openweathermap.org/data/2.5/forecast?id=4004867&appid=7a147d89f0a5ffb402d041e6623e2778&cnt=' + cnt)
            .then(res => res.json())
            .then((data) => {
                this.setState({ weatherList: data.list })
            })
            .catch(console.log)
    } 

    changeDays = (event: React.FormEvent<HTMLInputElement>) => {
        const target = event.target as HTMLInputElement;
        let days = target.value;
        this.setState({
          days: days
        })
    }    
}

export default Juarez;