import React, { Component } from 'react';
import Menu from '../../components/Menu/Menu';
import Routes from '../../components/Routes/Routes';
import { BrowserRouter } from 'react-router-dom';

class App extends Component {


  render() {
    return (
      <div>
        <BrowserRouter>
          <Menu />
          <Routes />
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

